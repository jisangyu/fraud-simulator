#!/bin/bash

#SBATCH -J staticQ
#SBATCH -p normal
#SBATCH --array=1-64
#SBATCH -c 1
#SBATCH --mem=2G
#SBATCH -t 1000:00
#SBATCH --mail-type=ALL
#SBATCH -o ./temp/static_deterministic_QLearning/sim-%j-%a.out
#SBATCH --mail-user=jisangyu@stanford.edu

# define some variables
nth_experiment=1
num_rounds=10000000
seed_diff=100000
seed0=$((${SLURM_ARRAY_TASK_ID} + ${nth_experiment} * 256))
seed1=$((${seed0} + ${seed_diff}))
seed2=$((${seed0} + 2*${seed_diff}))

# For safety, we deactivate any conda env that might be activated on interactive yens before submission and purge all loaded modules
source deactivate
module purge

# Load software
module load anaconda3

# Activate the environment
source activate env_donkor

# Run python script
echo python code/python/static_deterministic_QLearning_3firms.py ${seed0} ${seed1} ${seed2} ${num_rounds}
python code/python/static_deterministic_QLearning_3firms.py ${seed0} ${seed1} ${seed2} ${num_rounds}