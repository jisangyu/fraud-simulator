#!/bin/bash

#SBATCH -J staticQ
#SBATCH -p normal
#SBATCH --array=1-256
#SBATCH -c 1
#SBATCH --mem=2G
#SBATCH -t 2879:00
#SBATCH -o temp/static_deterministic_QLearning/sim-%j-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

# define some variables
nth_experiment=1
num_rounds=2000000
seed_diff=100000
seed0=$((${SLURM_ARRAY_TASK_ID} + ${nth_experiment} * 256))
seed1=$((${seed0} + ${seed_diff}))

# For safety, we deactivate any conda env that might be activated on interactive yens before submission and purge all loaded modules
source deactivate
module purge

# Load software
module load anaconda3

# Activate the environment
source activate env_donkor

# Run python script
echo python code/static_deterministic_QLearning.py ${seed0} ${seed1} ${num_rounds}
python code/static_deterministic_QLearning.py ${seed0} ${seed1} ${num_rounds}