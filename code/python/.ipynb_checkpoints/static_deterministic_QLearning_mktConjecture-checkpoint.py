import numpy as np
import typing
import math
import pandas as pd
import os
import sys
from itertools import product
from itertools import permutations
import json


os.chdir("/home/users/jisangyu/zfs/projects/faculty/jungho-idhlab/Eddie/fraud-simulator")
print(os.getcwd())

class ActionStateSpace:
    
    def __init__(self, cost, numAgents, numPartitions, memoryBound):
        self.cost = cost
        self.lowerBound = 0
        self.upperBound = 1/self.cost
        
        self.numAgents = numAgents
        self.numPartitions = numPartitions
        self.discreteActionSet = np.linspace(self.lowerBound, 
                                         self.upperBound,
                                         self.numPartitions)
        self.memoryBound = memoryBound
        
        # market conjecture bins
        self.marketConjectureBins = np.zeros(self.numPartitions)

        # first row is market conjecture bin, from second row starts history of actions
        self.currentState = np.stack((np.zeros(self.numPartitions), ) * self.memoryBound) # initialize
        self.currentState = np.insert(self.currentState, 0, self.marketConjectureBins, axis=0)
        
        # number of total states equals m^(k) where m is number of discretized actions, k is bounded memory length
        self.numStates = (self.numPartitions ** (self.memoryBound + 1))

        # index to stateMatrix mapping
        self.stateMatrices, self.state2Index = self.getState2IndexSpace()
    
    def getState2IndexSpace(self):
        
        # basic action element to iterate from; [1, 0, 0], [0, 1, 0], [0, 0, 1] for 3 actions
        
        eyeMatrix = np.eye(self.numPartitions, dtype=int).tolist()
        eyeMatrix = tuple(tuple(i) for i in eyeMatrix)
        
        stateMatrices = tuple(product(eyeMatrix, repeat=self.memoryBound+1)) # +1 for row that corresponds to marketConjectur Bin Vector
        

        state2Index = dict(zip(stateMatrices, range(len(stateMatrices))))
        if self.numStates != len(state2Index):
            raise Exception("Error: State Matrices are wrongly constructed")
        return stateMatrices, state2Index
    
    
class Firm(ActionStateSpace):
    
    def __init__(self, 
                 firmID,
                 seed,
                 mean_v = 0,
                 mean_n = 0,
                 sigma_v = 0.5, 
                 sigma_n = 0.25):
        super().__init__(cost, numAgents, numPartitions, memoryBound)
        self.firmID = firmID 
        self.randState = np.random.RandomState(seed=seed)
        self.mean_v = mean_v
        self.sigma_v = sigma_v
        self.var_v = sigma_v ** 2
        self.mean_n = mean_n
        self.sigma_n = sigma_n
        self.var_n = sigma_n ** 2 
        self.firmValue = self.randState.normal(mean_v, sigma_v)
        self.earningsNoise = self.randState.normal(mean_n, sigma_n)
        self.managerObservedEarnings = self.firmValue + self.earningsNoise
        # if e, v, n has to change in time t, move these to function instead of attributes

        self.Q_matrix = np.zeros((self.numStates, self.numPartitions)) # number of actions * states
    
    def chooseManipulationLevel(self, beta, t=0): # beta: decaying rate of epsilon (exploration)
        p = self.randState.random()
        self.epsilon = math.exp(-beta * t)
        print(f"Current (Decaying) Epsilon Level: {self.epsilon}")
        
        # explore vs. exploit
        if p < self.epsilon:
            self.manipulationIndex = self.randState.choice(np.arange(self.numPartitions))# inherited from ActionStateSpace
            print(f"Explore: Randomly Choose Action Index {self.manipulationIndex}")
        else:
            stateIndex = self.state2Index[tuple(map(tuple, self.currentState))]
            print(f"Exploit: choose the best action out of {self.Q_matrix[stateIndex,:]}")
            actionIndex = np.argmax(self.Q_matrix[stateIndex,:])
            print(f"Best Action Index {actionIndex}")
            self.manipulationIndex = actionIndex
            
        self.manipulationLevel = self.discreteActionSet[self.manipulationIndex]

        return self.manipulationIndex, self.manipulationLevel
    
    def manipulationCost(self):
        return 0.5*self.cost*(self.manipulationLevel**2)
    
    def changeActionIndex2Vector(self, actionIndex):
        vector = np.zeros(self.numPartitions)
        vector[actionIndex] = 1
        return vector
    
    def getNextStateMatrix(self, actionIndex, marketBiasVector):
        """
        update to next state matrix, and return before state matrix
        """
        actionVector = self.changeActionIndex2Vector(actionIndex)
        
        # update by moving memory by 1, and inserting new action vectors at the first index of each memoryBound index
        beforeState = self.currentState.copy()
        nextState = self.currentState.copy()
        # print(f"Original State Matrix before memory change {nextState}")
        
        # remove last row of current state (erased from memory due to bounded memory)
        nextState = nextState[: -1]
        
        
        # new action vector inserted at the second row
        nextState = np.insert(nextState, 1, actionVector, axis=0)
        
        
        ## change the first row (marketConjectureBias vector)
        nextState = nextState[1: ]
        nextState = np.insert(nextState, 0, marketBiasVector, axis=0)
        
        self.currentState = nextState
        
        return beforeState

        
    def updateQmatrix(self, firmIndex, beforeState, actionIndex, firmReward, alpha, delta):

        beforeStateQIndex = self.state2Index.get(tuple(map(tuple, beforeState)), -1)
        afterStateQIndex = self.state2Index.get(tuple(map(tuple, self.currentState)), -1)
        
        if beforeStateQIndex == -1:
            print("ValueError: Index Not Found because learning is at initial stages")
            return
        
        rewardPlusDiscountedFutureQ = firmReward + delta * np.max(self.Q_matrix[afterStateQIndex, :])
        
        self.Q_matrix[beforeStateQIndex, actionIndex] = (1 - alpha) * self.Q_matrix[beforeStateQIndex, actionIndex] + alpha * rewardPlusDiscountedFutureQ
        
class Market:
    
    def __init__(self):
        pass
    
    def conjectureBias(self, firmManipulationLevels: list):
        firmManipulationLevels = np.array(firmManipulationLevels)
        return np.mean(firmManipulationLevels)
    
    
class Env(ActionStateSpace):
    
    def __init__(self, market, firms: list, alpha=0.1, beta=0.00001, delta=0.95, seed=1234):
        super().__init__(cost, numAgents, numPartitions, memoryBound)
        self.firms = firms
        self.market = market
        self.alpha = alpha
        self.beta = beta
        self.delta = delta
        self.currentState = np.array([firm.currentState for firm in self.firms]) # state matrix of each firm, thus shape[0] = number of firms
    
    def getMarketConjecture(self, firmManipulationLevels):
        return self.market.conjectureBias(firmManipulationLevels)
    
    def changeBias2Vector(self, marketBias):
        '''
            use np.digitize to bin 0~1 bias level into self.numPartition bins
        '''
        vector = np.zeros(self.numPartitions)
        if marketBias == 0:
            vector[0] = 1
            return vector
        binIndex = np.digitize(marketBias, self.discreteActionSet, right=True)
        vector[binIndex] = 1
        return vector
    
    def updateFirmStateMatrix(self, actionIndices: list, marketBiasVector):
        # also return before states as we update
        beforeStates = []
        for idx, firm in enumerate(self.firms):
            firmActionIndex = actionIndices[idx]
            beforeState = firm.getNextStateMatrix(firmActionIndex, marketBiasVector)
            beforeStates.append(beforeState)
        return np.array(beforeStates)

        
    def rewardFirm(self, firmIndex, firm, manipulationLevel, marketBias):
        # Each firm's manager's observed Earnings (in chronological order this is before choosing manipulation level, but for programming purpose this goes latter)
        varRatio = firm.var_v / (firm.var_v + firm.var_n)
        
        # Each firm's manager's observed Earnings (in chronological order this is before choosing manipulation level, but for programming purpose this goes latter)
        # managerObsEarnings = firm.managerObservedEarnings
        # print(f"Firm {firmIndex} Observed Earnings: {managerObsEarnings}")
        
        firmManipulationCost = firm.manipulationCost()
        # print(f"Firm {firmIndex} Manipulation Cost: {firmManipulationCost}")
        firmRwd = varRatio * (manipulationLevel - 
                              marketBias) - firmManipulationCost
        print(f"Firm {firmIndex} Rwd: {firmRwd}")
        return firmRwd

    
    def runSingleRound(self, t):
        print("="*100)
        print(f"Round {t}")
        print(f"Current State Matrix {os.linesep}{self.currentState}")
        firmActions = [] # action set
        firmActionsIndex = [] # index of action set
        
        for idx, firm in enumerate(self.firms):    
            # Each firm chooses manipulation level (from Action set)
            manipulationIndex, manipulationLevel = firm.chooseManipulationLevel(self.beta, t) # specify beta
            firmActions.append(manipulationLevel)
            firmActionsIndex.append(manipulationIndex)
            
        # market conjectures the bias based on firms' manipulation levels (i.e. firm's actions)
        marketBias = self.getMarketConjecture(firmActions)
        
        # Env gives firm rewards; single interaction with env from firm's side
        firmRewards = []
        firmStateMatrices = []
        for idx, firm in enumerate(self.firms):    
            firmManipulationLevel = firmActions[idx]
            firmRwd = self.rewardFirm(idx, firm, firmManipulationLevel, marketBias)
            firmRewards.append(firmRwd)
        
        # update each firm's state matrix, save before state into beforeStates
        marketBiasVector = self.changeBias2Vector(marketBias)
        print(f"Market Bias {marketBias} changed to vector {marketBiasVector}")
        beforeStates = self.updateFirmStateMatrix(firmActionsIndex, marketBiasVector)
        self.currentState = np.array([firm.currentState for firm in self.firms])
        
        for idx, firm in enumerate(self.firms):
            beforeState = beforeStates[idx]
            actionIndex = firmActionsIndex[idx]
            firmReward = firmRewards[idx]
            firm.updateQmatrix(idx, beforeState, actionIndex, firmReward, self.alpha, self.delta)
        
        return firmActionsIndex, firmRewards
    
    def runExperiment(self, num_rounds, outpath, thresh_stop_convergence):
        # print(f"Start Simulation Experiment for {num_rounds} rounds")
        df = pd.DataFrame(columns=['P1Action', 'P2Action', 'P1Reward', 'P2Reward'])
        for t in range(num_rounds):
            firmActions, firmRewards = self.runSingleRound(t)
            df.loc[len(df)] = np.concatenate((firmActions, firmRewards))
               
            # stop iteration if convergence is reached for both players
            if t >= thresh_stop_convergence:
                df_tail = df.tail(thresh_stop_convergence)
                if len(df_tail['P1Action'].unique()) == 1 and len(df_tail['P2Action'].unique()) == 1:
                    print("Convergence Achieved for both players!")
                    break
                
        df.to_csv(outpath, index=False)
        return df
    
if __name__ == "__main__":
    seed0 = int(sys.argv[1])
    seed1 = int(sys.argv[2])
    num_rounds = int(sys.argv[3])

    cost = 1
    numAgents = 2
    numPartitions = 12 # number of discrete actions
    memoryBound = 2
    thresh_stop_convergence = 100

    firm0 = Firm(0, seed0)
    firm1 = Firm(1, seed1)

    firms = [firm0, firm1]
    market = Market()
    env = Env(market, firms)

    outpath = f"./out/static_deterministic_QLearning/mktConjBin_mgnRwd_{memoryBound}memory_{seed0}.csv"
    jsonpath = f"./out/static_deterministic_QLearning/mktConjBin_mgnRwd_{memoryBound}memory_{seed0}.json"
    print(outpath)
    df = env.runExperiment(num_rounds, outpath, thresh_stop_convergence)
    
    # save to json file
    
    json_item = {"alpha": env.alpha,
                 "beta" : env.beta,
                 "delta": env.delta,
                 "Q_matrices" : np.array([firm0.Q_matrix, firm1.Q_matrix]).tolist(),
                 "S_matrices" : env.currentState.tolist(),
                 "epsilon" : firm0.epsilon}
    
    with open(jsonpath, 'w') as filepath:
        json.dump(json_item, filepath, sort_keys=True, indent=4)